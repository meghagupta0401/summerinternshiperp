/**
 * 
 */
package utilities;

import javax.imageio.ImageIO;

/**
 * @author Joey
 *
 */

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import sun.misc.BASE64Decoder;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class ServletUtils {

	// constructors /////////////////////////////////////////////////////////////

	// constants ////////////////////////////////////////////////////////////////

	// classes //////////////////////////////////////////////////////////////////

	// methods //////////////////////////////////////////////////////////////////

	/**
	 * NOT UNIT TESTED Returns the URL (including query parameters) minus the scheme, host, and
	 * context path.  This method probably be moved to a more general purpose
	 * class.
	 */
	public static String getRelativeUrl(
			HttpServletRequest request ) {

		String baseUrl = null;

		if ( ( request.getServerPort() == 80 ) ||
				( request.getServerPort() == 443 ) )
			baseUrl =
			request.getScheme() + "://" +
					request.getServerName() +
					request.getContextPath();
		else
			baseUrl =
			request.getScheme() + "://" +
					request.getServerName() + ":" + request.getServerPort() +
					request.getContextPath();

		StringBuffer buf = request.getRequestURL();

		if ( request.getQueryString() != null ) {
			buf.append( "?" );
			buf.append( request.getQueryString() );
		}

		return buf.substring( baseUrl.length() );
	}

	/**
	 * NOT UNIT TESTED Returns the base url (e.g, <tt>http://myhost:8080/myapp</tt>) suitable for
	 * using in a base tag or building reliable urls.
	 */
	public static String getBaseUrl( HttpServletRequest request ) {
		if ( ( request.getServerPort() == 80 ) ||
				( request.getServerPort() == 443 ) )
			return request.getScheme() + "://" +
			request.getServerName() +
			request.getContextPath();
		else
			return request.getScheme() + "://" +
			request.getServerName() + ":" + request.getServerPort() +
			request.getContextPath();
	}

	/**
	 * Returns the file specified by <tt>path</tt> as returned by
	 * <tt>ServletContext.getRealPath()</tt>.
	 */
	public static File getRealFile(
			HttpServletRequest request,
			String path ) {

		return
				new File( request.getSession().getServletContext().getRealPath( path ) );
	}

	public static String getFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		System.out.println("content-disposition header= "+contentDisp);
		String[] tokens = contentDisp.split(";");
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				return token.substring(token.indexOf("=") + 2, token.length()-1);
			}
		}
		return "";
	}
	public static void writeOutputStream(String value, OutputStream outputStream) throws IOException {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] imgBytes = decoder.decodeBuffer(value);
		BufferedImage bufImg = ImageIO.read(new ByteArrayInputStream(imgBytes));
		ImageIO.write(bufImg, "png", outputStream);
	
	}
	public static InputStream getDecodedBase64Stream(String value) throws IOException {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] imgBytes = decoder.decodeBuffer(value);
		return new ByteArrayInputStream(imgBytes);
	
	
	}
	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			InputStreamReader input = new InputStreamReader(is);
			br = new BufferedReader(input);
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
          input.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					
					br.close();
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
       try {
    	  
		is.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		return sb.toString();

	}

}




