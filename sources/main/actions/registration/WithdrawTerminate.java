package actions.registration;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WithdrawTerminate
 */
@WebServlet("/WithdrawTerminate")
public class WithdrawTerminate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WithdrawTerminate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("action").equals("withdraw")){
			long  reg_id=Long.parseLong(request.getParameter("registration_id"));
			postgreSQLDatabase.registration.Query.updateVerificationStatus(7, reg_id);
		}
		if(request.getParameter("action").equals("terminate")){
			long reg_id=Long.parseLong(request.getParameter("registration_id"));
			postgreSQLDatabase.registration.Query.updateVerificationStatus(8, reg_id);
		}
	}

}
