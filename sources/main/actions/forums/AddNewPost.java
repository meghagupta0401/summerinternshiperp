package actions.forums;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import postgreSQLDatabase.notifications.Notifications;



/**
 * Servlet implementation class AddNewPost
 */
@WebServlet("/AddNewPost")
public class AddNewPost extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewPost() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Long category_id=Long.parseLong(request.getParameter("category_id"));
		//int count=0;
		try{
		String post_name=request.getParameter("post_name");
		Long thread_id=Long.parseLong(request.getParameter("thread_id"));
		String thread_name=postgreSQLDatabase.forum.Query.getThreadName(thread_id);
		HttpSession session=request.getSession();
		Long author_id=Long.parseLong(String.valueOf(session.getAttribute("erpId")));
		String author_name=postgreSQLDatabase.authentication.Query.getUserUsername(author_id);
	    postgreSQLDatabase.forum.Query.addPost(thread_id, author_id, post_name);
		Notifications notif=new Notifications();
		notif.setAuthor(author_name);
		notif.setLink("allPosts.jsp?thread_id="+thread_id);
		//SimpleDateFormat date_format = new SimpleDateFormat("mm-dd-yyyy hh:MM:ss.SSSSSS");
		 //notif.setTimestamp(Date.valueOf(date_format.format(new Date(0))));
		notif.setTimestamp(utilities.StringFormatter.convert(new java.util.Date()));
		notif.setType("Forum");
		notif.setMessage("New post in thread "+thread_name);
		notif.setExpiry(utilities.StringFormatter.convert(new java.util.Date()));
		ArrayList<Long> subscribers=postgreSQLDatabase.forum.Query.getAllSubscribersOfThread(thread_id);
		subscribers.remove(author_id);
		postgreSQLDatabase.notifications.Query.addGroupNotification(notif, subscribers);
		
		
		//response.sendRedirect("student/allPosts.jsp?thread_id="+thread_id);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
