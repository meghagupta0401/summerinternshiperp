<html>
<head>
<style>
tr, td, th {
	border: 1px solid black;
}
</style>
</head>
<body>
	<p align="center">
		<img alt=""
			src="http://172.16.1.231/ftp/templates/IIITKLetterHead.png"
			style="height: 99px; width: 729px" />
	</p>



	
		<h3 style="text-align:center">Documents Checklist</h3>
	
	<table style="border: none;!important">
		<tr>
			<td style="border: none;!important">Student Name:</td>
			<td style="border: none;!important">_______________________</td>
		</tr>
		<tr style="border: none;!important">
			<td style="border: none;!important">Student Id:</td>
			<td style="border: none;!important">_______________________</td>
		</tr>

	</table>
	<br/>
	<table style="border: 1px solid black; border-collapse: collapse;">
		<tr>
			<th width="10%">S. No.</th>
			<th width="50%">Particular</th>
			<th width="20%">Verified</th>
			<th width="20%">Pending</th>
		</tr>
		<tr>
			<td>1.</td>
			<td>Provisional Allotment letter issued by CSAB/JOSSA</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>2.</td>
			<td>SAT score proof in hard copy(print out)/JEE Mains score
				card/Rank Card</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Date of birth certificate (X class pass
				certificate/marksheet)</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Marksheets ,certificate &amp; Degree of qualifying
				examination</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Character certificate from the last school or institute
				attended in original</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>6.</td>
			<td>Transfer certificate/Migration Certificate submitted in
				original</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>7.</td>
			<td>Medical certificate issued by recognized/reputed hospital</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>8.</td>
			<td>Caste Certifiacte(SC/ST/OBC)</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>9.</td>
			<td>Certificate of physical handicapped(if applicable)</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>10.</td>
			<td>ID Proof In case of Indian student-Aadhaar card/voter ID card</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>11.</td>
			<td>Gap certificate</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>12.</td>
			<td>NOC from employer(application in case of M.Tech/M.Plan/Ph.D. sponsored candidate)</td>
			<td></td>
			<td></td></tr>
	</table>
	<br/>
	<table>
		<tr>
			<td style="border: none !important;"><b>Checked
					by.................................................................</b></td>
			<td style="border: none !important;"><b>Verified
					by.................................................................</b></td>
					</tr>
		<tr style="border: none;">
			<td style="border: none !important;">Name
				.................................................</td>
			<td style="border: none !important;">Name.........................................................................
			</td>
		</tr>
		<tr style="border: none;">
			<td style="border: none !important;"></td>
			<td style="border: none !important;">Officer Incharge</td>
		</tr>
	</table>
</body>
</html>
